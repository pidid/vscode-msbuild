<?xml version="1.0" encoding="utf-8"?>
<!--
***********************************************************************************************
Microsoft.TypeScript.targets

WARNING:  DO NOT MODIFY this file unless you are knowledgeable about MSBuild and have
          created a backup copy.  Incorrect changes to this file will make it
          impossible to load or build your web deploy projects from the command-line or the IDE.

This file defines the steps in the standard build process for TypeScript files.

Copyright (C) Microsoft Corporation. All rights reserved.
***********************************************************************************************
-->
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <TypeScriptSdkDir>$(MSBuildProgramFiles32)\Microsoft SDKs\TypeScript</TypeScriptSdkDir>
    <PreferredUILang Condition="'$(BuildingInsideVisualStudio)' == 'true' and '$(PreferredUILang)' == ''">$([System.Globalization.CultureInfo]::CurrentUICulture.Name)</PreferredUILang>    
  </PropertyGroup>

  <!-- Import all the props under the versions folder -->
  <!-- This will be sorted, see https://msdn.microsoft.com/en-us/library/92x05xfs.aspx#Anchor_3 -->
  <Import Project="$(TypeScriptSdkDir)\versions\*" />

  <PropertyGroup Label="TypeScriptToolsVersionHandling">
    <!-- If the user specified "Latest", pick the latest available -->
    <TypeScriptEffectiveToolsVersion Condition="'$(TypeScriptToolsVersion)' == 'Latest'">$(LastKnownTypeScriptVersion)</TypeScriptEffectiveToolsVersion>
    <TypeScriptEffectiveToolsVersion Condition="'$(TypeScriptToolsVersion)' != 'Latest'">$(TypeScriptToolsVersion)</TypeScriptEffectiveToolsVersion>

    <!-- Prefer NuGet tools version -->
    <TypeScriptEffectiveToolsVersion Condition="'$(TypeScriptNuGetToolsVersion)'!=''">$(TypeScriptNuGetToolsVersion)</TypeScriptEffectiveToolsVersion>

    <!-- If the user didn't specify a TypeScriptToolsVersion, set the warning state -->
    <TypeScriptVersionCheckResult Condition="'$(TypeScriptEffectiveToolsVersion)' == ''">NoneSpecified</TypeScriptVersionCheckResult>

    <!-- If the user specified a specific version and it's found -->
    <TypeScriptVersionCheckResult Condition="'$(TypeScriptEffectiveToolsVersion)' != '' AND Exists('$(TypeScriptSdkDir)\$(TypeScriptEffectiveToolsVersion)\tsc.exe')">Good</TypeScriptVersionCheckResult>

    <!-- If the user specified a specific version but that version isn't installed, use the latest version and set the warning state -->
    <TypeScriptVersionCheckResult Condition="'$(TypeScriptEffectiveToolsVersion)' != '' AND !Exists('$(TypeScriptSdkDir)\$(TypeScriptEffectiveToolsVersion)\tsc.exe') AND
      '$(TypeScriptEffectiveToolsVersion)' &gt; '$(LastKnownTypeScriptVersion)' ">Downgrade</TypeScriptVersionCheckResult>
    <TypeScriptVersionCheckResult Condition="'$(TypeScriptEffectiveToolsVersion)' != '' AND !Exists('$(TypeScriptSdkDir)\$(TypeScriptEffectiveToolsVersion)\tsc.exe') AND
      '$(TypeScriptEffectiveToolsVersion)' &lt; '$(LastKnownTypeScriptVersion)' ">Upgrade</TypeScriptVersionCheckResult>

    <!-- If we're using a Nuget package, don't care whether the SDK is found or not -->
    <TypeScriptVersionCheckResult Condition="'$(TypeScriptNuGetToolsVersion)'!=''">Good</TypeScriptVersionCheckResult>
    
    <!-- In case of a problem with the user-specified tools version, fall back to the latest available version -->
    <TypeScriptEffectiveToolsVersion Condition="$(TypeScriptVersionCheckResult) != 'Good' ">$(LastKnownTypeScriptVersion)</TypeScriptEffectiveToolsVersion>
  </PropertyGroup>

  <Target Name="ReportTypeScriptVersion" BeforeTargets="Build" Condition="'@(ConfigFiles)' != '' OR '@(TypeScriptCompile)' != ''">
    <FormatLocalizedString Condition="'$(TypeScriptVersionCheckResult)' == 'NoneSpecified'"
      Culture="$(PreferredUILang)" 
      Name="TypeScriptNoVersionWarning" 
      Arguments="$(LastKnownTypeScriptVersion)">
      <Output TaskParameter="String" PropertyName="TypeScriptNoVersionWarning" />
    </FormatLocalizedString>

    <FormatLocalizedString Condition="'$(TypeScriptVersionCheckResult)' == 'Downgrade' OR '$(TypeScriptVersionCheckResult)' == 'Upgrade'"
      Culture="$(PreferredUILang)" 
      Name="TypeScriptVersionMismatchWarning" 
      Arguments="$(TypeScriptToolsVersion);$(LastKnownTypeScriptVersion)">
      <Output TaskParameter="String" PropertyName="TypeScriptVersionMismatchWarning" />
    </FormatLocalizedString>

    <Warning Condition="'$(TypeScriptVersionCheckResult)' == 'NoneSpecified'" Text="$(TypeScriptNoVersionWarning)" />
    <Warning Condition="'$(TypeScriptVersionCheckResult)' == 'Downgrade' OR '$(TypeScriptVersionCheckResult)' == 'Upgrade'" Text="$(TypeScriptVersionMismatchWarning)" />
  </Target>

   <!-- Now import the actual version specific typescript targets/props -->
   
   <!-- Give preference to TypeScriptToolsVersion if defined by user -->
   <Import Condition="$(EnableTypeScriptNuGetTarget) == '' AND ($(TypeScriptToolsVersion) != '' AND Exists('$(TypeScriptSdkDir)\$(TypeScriptToolsVersion)\build\Microsoft.TypeScript.targets'))"
		   Project="$(TypeScriptSdkDir)\$(TypeScriptToolsVersion)\build\Microsoft.TypeScript.targets" />

   <Import Condition="$(EnableTypeScriptNuGetTarget) == '' AND ($(TypeScriptToolsVersion) == '' OR !Exists('$(TypeScriptSdkDir)\$(TypeScriptToolsVersion)\build\Microsoft.TypeScript.targets'))"
      Project="$(TypeScriptSdkDir)\$(LastKnownTypeScriptVersion)\build\Microsoft.TypeScript.targets" />
</Project>