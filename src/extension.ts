'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { debug } from 'util';

//Threw the buildtype logic in a hurry. If there is a better solution, please implement it.
var buildTypes = {
    Build: "Build",
    Rebuild: "Rebuild",
    Clean: "Clean"
}

export function activate(context: vscode.ExtensionContext) {

    console.log('MSBuild extension is active...');
    var terminal: vscode.Terminal;

    function msbuild(file: vscode.Uri, buildType: string) {
        var path = context.extensionPath;
        var msbuild = path + '\\lib\\Community\\MSBuild\\15.0\\Bin\\MSBuild.exe';

        if(!terminal)
            terminal = vscode.window.createTerminal('msbuild');

        //This command was fired via context menu item.
        if(file) {
            var filePath = file.path;
            
            //Having a forward slash will make msbuild complain and not build.
            if(filePath.charAt(0) == '/')
                filePath = filePath.substring(1, filePath.length);

            // vscode.window.showInformationMessage(filePath);
            // vscode.window.showInformationMessage('Right clicked');
            msbuild += ' ' + filePath;
        }

        msbuild += ' /t:' + buildType;

        terminal.processId.then(x => {
            vscode.window.showInformationMessage('MSBuild ' + buildType.toLocaleLowerCase() + 'ing...');
        });

        terminal.sendText(msbuild);
        terminal.show();
    }

    //We seperate the commands like this so we can do middleware work and to support multiple context menu items 
    //(each context menu item can only have one commane)
    let build = vscode.commands.registerCommand('extension.Build', (file) => {
        msbuild(file, buildTypes.Build);
    });

    let rebuild = vscode.commands.registerCommand('extension.Rebuild', (file) => {
        msbuild(file, buildTypes.Rebuild);
    });

    let clean = vscode.commands.registerCommand('extension.Clean', (file) => {
        msbuild(file, buildTypes.Clean);
    });

    context.subscriptions.push(build);
    context.subscriptions.push(rebuild);
    context.subscriptions.push(clean);
}

// this method is called when your extension is deactivated
export function deactivate() {
}